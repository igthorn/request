
"use strict";

var _            = require("underscore");
var RequestError = require("./requestError");

/**
 * Request class provides deep validation of passed data
 */
class Request
{
  /**
   * Custom constructor sets supported logical, order and comparision
   * operators
   */
  constructor(requestData)
  {
    this.supportedLogicOperators = [
      "and",
      "or"
    ];

    this.supportedOrderOperators = [
      "asc",
      "desc"
    ];

    this.supportedComparisionOperators = [
      "eq",      // Matches values that are exactly the same as the value specified in the query.
      "gt",      // Matches values that are greater than the value specified in the query.
      "gte",     // Matches values that are greater than or equal to the value specified in the query.
      "lt",      // Matches values that are less than the value specified in the query.
      "lte",     // Matches values that are less than or equal to the value specified in the query.
      "ne",      // Matches all values that are not equal to the value specified in the query.
      "in",      // Matches values that do exist in an array specified to the query.
      "nin",     // Matches values that do not exist in an array specified to the query.
      "regex",   // Matches values using a regular expression on the specified query.
      "like",    // Matches values that are matching pattern passed in the value specified in the query.
      "contains" // Matches one of values in passed key which needs to be array
    ];

    this.supportedOperators = this.supportedOrderOperators.concat(
      this.supportedComparisionOperators
    );

    this.defaultResultsLimit = 50;
    this.reset();

    if (requestData) {
      this.inflate(requestData);
    }
  }

  /**
   * Method inflates an Request object using passed data
   *
   * @param  object data Assoc array of conditions, fields, joins etc.
   * @return Request this Fluent interface
   */
  inflate(data)
  {
    if (_.has(data, "fields")) {
      this.setFields(data.fields);
    }

    if (_.has(data, "without")) {
      this.setWithout(data.without);
    }

    if (_.has(data, "joins")) {
      this.setJoins(data.joins);
    }

    if (_.has(data, "conditions")) {
      this.setConditions(data.conditions);
    }

    if (_.has(data, "order")) {
      this.setOrder(data.order);
    }

    if (_.has(data, "limit")) {
      this.setLimit(data.limit);
    }

    if (_.has(data, "offset")) {
      this.setOffset(data.offset);
    }

    if (_.has(data, "storage")) {
      this.setStorage(data.storage);
    }

    if (_.has(data, "count")) {
      this.enableCount();
    }

    return this;
  }

  /**
   * Method sets list of fields to be retrieved
   * Converts 0: id to id: id
   *
   * @param array|object|string fields List of fields or single field
   * @return Request this Fluent interface
   */
  setFields(fields)
  {
    if (_.isString(fields)) {
      fields = [fields];
    }

    if ((!_.isArray(fields) && !_.isObject(fields)) || _.isEmpty(fields)) {
      throw new RequestError(
        "Invalid list of fields provided. String|Object|Array expected",
        fields,
        2
      );
    }

    let filtered = {};
    _(fields).forEach(function(field, index) {

      if (!_.isString(field) || _.isEmpty(field)) {

        let type = "field";
        if (isNaN(index)) {
          type = "field alias";
        }

        throw new RequestError(
          "Invalid " + type + " passed: '" + field + "'",
          {
            field : field
          },
          3
        );
      }

      if (_.isString(index) && _.isEmpty(index)) {
        throw new RequestError(
          "Invalid field passed: '" + index + "'",
          {
            index: index
          },
          4
        );
      }

      if (isNaN(index)) {
        filtered[index] = field;
      } else {
        let aliasedField = field;
        if (field.indexOf(".") > -1) {
          aliasedField = field.split(".").pop();
        }

        filtered[field] = aliasedField;
      }

    });

    this.query.fields = filtered;
    this.usedFields = _.union(this.usedFields, _.keys(filtered));

    return this;
  }

  /**
   * Method sets list of fields to be excluded from retrieval
   * Converts 0: id to id: id
   *
   * @param array|object|string without List of fields or single field
   * @return Request this Fluent interface
   */
  setWithout(without)
  {
    if (_.isString(without)) {
      without = [without];
    }

    if ((!_.isArray(without) && !_.isObject(without)) || _.isEmpty(without)) {
      throw new RequestError(
        "Invalid list of fields provided. String|Object|Array expected",
        {
          without: without
        },
        2
      );
    }

    let filtered = {};
    _(without).forEach(function(field, index) {
      if (!_.isString(field) || _.isEmpty(field)) {
        let type = "field";
        if (isNaN(index)) {
          type = "field alias";
        }

        throw new RequestError(
          "Invalid " + type + " passed: '" + field + "'",
          {
            field : field
          },
          3
        );
      }

      if (_.isString(index) && _.isEmpty(index)) {
        throw new RequestError(
          "Invalid field passed: '" + index + "'",
          {
            index: index
          },
          4
        );
      }

      if (isNaN(index)) {
        filtered[index] = field;
      } else {
        let aliasedField = field;
        if (field.indexOf(".") > -1) {
          aliasedField = field.split(".").pop();
        }

        filtered[field] = aliasedField;
      }

    });

    this.query.without = filtered;

    return this;
  }

  /*
   * Method gets list of fields to be retrieved
   *
   * @return object fields List of fields
   */
  getFields()
  {
    return this.query.fields;
  }

  /*
   * Method gets list of fields to exclude when retrieving
   *
   * @return object fields List of fields
   */
  getWithout()
  {
    return this.query.without;
  }

  /**
   * Method sets resource"s relation(s) to be joined
   * Expected structure:
   * [
   *   "creator"
   * ]
   * @param array relations List of resource"s relation(s)
   * @return Request this Fluent interface
   */
  setJoins(relations)
  {
    if (_.isString(relations)) {
      relations = [relations];
    }

    if (!_.isArray(relations) || _.isEmpty(relations)) {
      throw new RequestError(
        "Invalid join relations list provided",
        relations,
        5
      );
    }

    _.forEach(relations, function(relation) {
      if (!_.isString(relation) || _.isEmpty(relation)) {
        throw new RequestError(
          "Invalid join relation provided: '" + relation + "'",
          {
            relation: relation
          },
          6
        );
      }
    });

    this.query.joins = relations;
    return this;
  }

  /**
   * Method returns resources to be joined
   * Expected structure:
   * [
   *   owner
   * ]
   * @return array resources List of resources
   */
  getJoins()
  {
    return this.query.joins ? this.query.joins : [];
  }

  /**
   * Method sets query's conditions
   *
   * @param object data Conditions details field => value|condition
   * @return Request this Fluent interface
   */
  setConditions(data)
  {
    this.validateCondtionsLevel(data, 1);

    // if nothing above thrown an error we can safely assign the conditions
    this.query.conditions = data;

    return this;
  }

  /**
   * Method gets query's conditions
   *
   * @param object data Conditions details field => value|condition
   * @return Request this Fluent interface
   */
  getConditions()
  {
    return this.query.conditions;
  }

  /**
   * Method append condition to query's conditions
   *
   * @param object condition Condition
   * @return Request this Fluent interface
   * @throws RequestError error Validation error
   */
  appendConditions(conditions)
  {
    this.validateCondtionsLevel(conditions, 1);

    // if nothing above thrown an error we can safely assign the conditions
    this.query.conditions = _.extend(this.query.conditions, conditions);

    return this;
  }

  /**
   * Recursive method validates single condition level
   *
   * @param data  object  Validated condition level
   * @param level integer Level of recursion (how deep condition is)
   */
  validateCondtionsLevel(data, level)
  {
    if (!_.isObject(data) || _.size(data) === 0 || _.isArray(data)) {
      throw new RequestError(
        "Invalid data structure provided",
        {
          level: level,
          data: data
        },
        7
      );
    }

    var value = null;

    for (var index in data) {

      value = data[index];

      // if it"s logic it should point to array with more than 1 element
      if (index === "and" || index === "or") {

        if (!_.isArray(value)) {
          throw new RequestError(
            "Logic operator expects array of conditions",
            value,
            8
          );
        }

        if (value.length < 2) {
          throw new RequestError(
            "Logic operator expects array of at least two conditions",
            value,
            9
          );
        }

        if (level > 3) {
          throw new RequestError(
            "Only 3 levels of nested conditions are supported",
            data,
            4
          );
        }

        var singleCondtion = null;
        for (var tempIndex in value) {
          singleCondtion = value[tempIndex];
          level += 1;
          this.validateCondtionsLevel(singleCondtion, level);
        }

      } else {

        // now we have something like that
        // {
        //   field : "value",
        // }
        // or
        // {
        //   field2 : {
        //     "gt" : "value2"
        //   }
        // }
        // or
        // {
        //   field3 : ["abc", "def"]
        // }
        // or mix of 2 above
        // index is a field
        var field = index;

        this.usedFields = _.union(this.usedFields, [field]);

        // if type 1 it"s ok!
        if (_.isString(value) || _.isNumber(value)) {
          continue;
        }

        // if it's type 2 validate that contains only strings etc
        if (_.isArray(value)) {
          var tempValue = null;
          for (var tempIndex in value) {
            tempValue = value[tempIndex];
            if (!_.isString(tempValue) && !_.isNumber(tempValue)) {
              throw new RequestError(
                "Invalid list of values for field: '" + field + "'",
                {
                  field : field,
                  value : tempValue
                },
                10
              );
            }
          }
          continue;
        }

        if (_.isObject(value)) {
          var tempValue = null;
          for (var tempIndex in value) {
            tempValue = value[tempIndex];

            if (_.isArray(tempValue)) {

              if (!_.contains(this.supportedComparisionOperators, tempIndex)) {
                throw new RequestError(
                  "Invalid operator provided for field: '" + field + "'",
                  {
                    field : field
                  },
                  12
                );
              }

              var i = null;
              for (var v in tempValue) {
                i = tempValue[v];
                // each value can be only string or integer
                if (!_.isString(i) && !_.isNumber(i)) {
                  throw new RequestError(
                    "Invalid list of values for field: '" + field + "' and operator: '" + tempIndex + "'",
                    {
                      field : field,
                      operator : tempIndex
                    },
                    11
                  );
                }
              }
              continue;
            }

            // each index should be valid operator
            if (!_.contains(this.supportedComparisionOperators, tempIndex)) {
              throw new RequestError(
                "Invalid operator provided for field: '" + field + "'",
                {
                  field : field
                },
                12
              );
            }

            // each value can be only string or integer
            if (!_.isString(tempValue) && !_.isNumber(tempValue)) {
              throw new RequestError(
                "Invalid list of values for field: " + field + " and operator: " + tempIndex,
                {
                  field : field,
                  opeator : tempIndex
                },
                13
              );
            }
          }

          continue;

        }

        throw new RequestError(
          "Invalid data structure provided for field: '" + field + "'",
          value,
          14
        );

      }
    }
  }

  /**
   * Sets query order
   *
   * Accepted object:
   * {
   *   "field1" : "desc"
   * }
   *
   * or
   *
   * [
   *   "field1"
   * ]
   *
   * or
   *
   * field1
   *
   * @param object order Order description
   * @return Request this Fluent interface
   */
  setOrder(order)
  {
    if (_.isString(order)) {
      order = {
        [order]: "asc"
      };
    }

    if (_.isArray(order)) {
      let newOrder = {};
      _(order).forEach(function(orderField) {
        if (!_.isString(orderField)) {
          throw new RequestError(
            "Invalid order field(s) passed",
            order,
            15
          );
        }
        newOrder[orderField] = "asc";
      });
      order = newOrder;
    }

    if (!_.isObject(order)) {
      throw new RequestError(
        "Invalid order passed",
        {
          order: order
        },
        15
      );
    }

    var that = this;
    _.forEach(order, function(orderOperator, fieldName) {
      if ((!_.isString(orderOperator) ||
        !_.contains(that.supportedOrderOperators, orderOperator)
      ) && (!_.isString(fieldName) ||
        !_.contains(that.supportedOrderOperators, fieldName)
      )) {
        throw new RequestError(
          "Invalid order operator passed",
          {
            orderOperator: orderOperator
          },
          16
        );
      }
      that.usedFields = _.union(that.usedFields, [fieldName]);
    });

    this.query.order = order;
  }

  /**
   * Getter for query order
   *
   * @return object Order description
   */
  getOrder()
  {
    return this.query.order;
  }

  /**
   * Method disables limit on query
   */
  disableLimit()
  {
    this.query.limit = false;
    return this;
  }

  /**
   * Method validates and sets limit for query
   *
   * @param integer limit Limit to be set in query
   * @return Request this Fluent interface
   * @throws RequestError When passed limit is invalid
   */
  setLimit(limit)
  {
    if ((!_.isNumber(limit) && !_.isString(limit)) ||
      parseInt(limit, 10) != limit || limit < 1)
    {
      throw new RequestError(
        "Invalid limit passed. Positive integer value expected",
        limit,
        17
      );
    }

    this.query.limit = parseInt(limit, 10);

    return this;
  }

  /**
   * @return number Query Limit
   */
  getLimit()
  {
    return this.query.limit;
  }

  /**
   * Method validates and sets offset for query
   *
   * @param integer offset Offset value
   * @return Request this Fluent interface
   * @throws RequestError When passed limit is invalid
   */
  setOffset(offset)
  {
    if ((!_.isNumber(offset) && !_.isString(offset)) ||
      parseInt(offset, 10) != offset || offset < 1)
    {
      throw new RequestError(
        "Invalid offset passed. Positive integer value expected",
        {
          offset: offset
        },
        18
      );
    }

    this.query.offset = parseInt(offset, 10);

    return this;
  }

  /**
   * @return number Offset value
   */
  getOffset()
  {
    return this.query.offset;
  }

  /**
   * Method validates and sets storage type for query
   *
   * @param string storage Storage type
   * @return Request this Fluent interface
   * @throws RequestError When invalid storage type was invalid
   */
  setStorage(storage)
  {
    var validStorageTypes = ["default", "cache", "db", "memory"];

    if (!_.isString(storage) || validStorageTypes.indexOf(storage) === -1) {
      throw new RequestError(
        "Invalid storage type passed. default|cache|db|memory expected",
        storage,
        19
      );
    }

    this.query.storage = storage;

    return this;
  }

  /**
   * @return number storage type
   */
  getStorage()
  {
    return this.query.storage;
  }

  /**
   * Method returns fields used in query
   *
   * @return Array List of fields used in query
   */
  getUsedFields()
  {
    return this.usedFields;
  }

  /**
   * Method enables count
   *
   * @return self this Fluent interface
   */
  enableCount()
  {
    this.query.count = true;
    return this;
  }

  /**
   * Method disables count
   *
   * @return self this Fluent interface
   */
  disableCount()
  {
    this.query.count = false;
    return this;
  }

  /**
   * Method returns result of check is count enabled
   *
   * @return bool result Result of check
   */
  isCountEnabled()
  {
    return this.query.count;
  }

  /**
   * Method returns array representation of query
   * @return object query Object describing query
   */
  export()
  {
    return this.query;
  }

  /**
   * Method clears current state of Request entity
   *
   * @return Request this Fluent interface
   */
  reset()
  {
    this.query = {
      conditions: {},
      limit: this.defaultResultsLimit,
      storage: "default",
      count: undefined
    };

    this.usedFields = [];

    return this;
  }
}

module.exports = Request;
