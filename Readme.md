## Igthorn Request

[![Igthorn](http://igthorn.com/assets/images/igthorn_resized.png "Igthorn")](http://igthorn.com "Igthorn")

Generic API request class that allows to validate query.

[![Build Status](https://gitlab.com/igthorn/request/badges/develop/build.svg)](https://gitlab.com/igthorn/request/builds)

Igthorn Request provides two different features:
- request class to parse, validate and create API requests
- express middleware to parse and validate requests

It's part of [Igthorn](http://igthorn.com "Igthorn") project.

### Usage

Typical usage would be to add parsing request as middleware:

```
var express           = require("express");
var app               = express();
var RequestMiddleware = require("igthorn-request").RequestMiddleware;
app.all("*", RequestMiddleware.validate);

// after that middleware you can refer to parsed request in following way

get(request, response)
{
  let parsedRequest = request.getParsedRequest();

  // ...
}

```

Request can also be used as a standalone class:

```
var Request = require("igthorn-request");
var request = new Request();

request.inflate({
  conditions: {
    price: {
      "gt" : 15,
      "lte": 70
    },
    "category": [21, 45]
  }
});
```

### Request query structure

Request class is capable of handling following params:
- fields
  List of fields to be retrieved
  ```
  "name"
  // or
  ["firstName", "lastName"]
  // or
  {
    "name" : "firstName"
  }
  ```
- without
  List of fields to be ignored when retrieving data
  ```
  "name"
  // or
  ["firstName", "lastName"]
  ```
- joins
  List of relations(referred by name) to be fetched in addition to data from main resource
  ```
  "comments"
  ```
- conditions
  Mongo-like structure allowing multi level nesting - see examples below
  ```
  {
    name: "John"
  }
  // or
  { // implicit logical and
    name: "John",
    surname: "Smith"
  }
  // or
  { // explicit logic and
    "and" : [
      {name: "John"}
      {surname: "Smith"}
    ]
  }
  // or
  {
    "or": [
      { // implicit logical and
        name: "John",
        surname: "Smith"
      },
      {
        "age" : 20
	  }
    ]
  }
  // or
  { // different operators
    "age" : {
      "gt" : 20,
      "lt" : 25
	}
  }
  ```

| operator   | meaning                                                                              |
| ---------- | ------------------------------------------------------------------------------------ |
|  "eq",     | Matches values that are exactly the same as the value specified in the query.        |
|  "gt",     | Matches values that are greater than the value specified in the query.               |
|  "gte",    | Matches values that are greater than or equal to the value specified in the query.   |
|  "lt",     | Matches values that are less than the value specified in the query.                  |
|  "lte",    | Matches values that are less than or equal to the value specified in the query.      |
|  "ne",     | Matches all values that are not equal to the value specified in the query.           |
|  "in",     | Matches values that do exist in an array specified to the query.                     |
|  "nin",    | Matches values that do not exist in an array specified to the query.                 |
|  "regex",  | Matches values using a regular expression on the specified query.                    |
|  "like",   | Matches values that are matching pattern passed in the value specified in the query. |
| "contains" | Matches one of values in passed key which needs to be array                          |

- order
  Allows to set ASC/DESC multi field order
  ```
  "name"
  // or
  [
    "name",
    "age"
  ]
  // or
  {
    "name": "desc",
    "age": "asc"
  }
  ```
- limit - limits number of retrieved resources - numeric value
- offset - specifies offset of skipped resources - numeric value
- storage - special storage name that allows API user to dictate where data should be stored/come from


### Methods

Request class implements following methods:

* inflate(data:object) : self

  parses, validates and inflates request object with passed data object
* setFields(fields:string|array|object) : self

  sets fields to be queried
* setWithout(fields:string|array|object) : self

  sets fields to exclude from result
* getFields() : object

  gets list of fields to be queried
* getWithout() : object

  gets list to exclude from result
* setJoins(joins:string|array) : self

  sets list of relationships to be joined to resource, allows for either string or array
* getJoins() : array

  gets list of relationships to be joined to resource
* setConditions(data:object) : self

  parses, validates and sets passed conditions
* getConditions() : object

  gets conditions
* appendConditions(data:object) : self

  parses, validates and appends passed conditions to current conditions
* setOrder(order:string|array|object) : self

  parses, validates and sets order
* getOrder() : object

  gets order
* setLimit(limit:int) : self

  sets limit of resources to be returned
* getLimit() : number

  gets limit of resources to be returned
* disableLimit() : self

  disables limit
* setOffset(offset:int|string) : self

  sets offset
* getOffset() : int

  gets offset
* setStorage(storage:string) : self

  sets storage to be used
* getStorage() : string

  get storage to be used
* enableCount() : self

  enables count to be returned only for that query
* disableCount() : self

  disables count to be returned only for that query
* isCountEnabled() : boolean

  gets flag is count enabled
* getUsedFields() : array

  gets list of fields used acros query
* export() : object

  exports object representation of request
* reset() : self

  sets request object state to it's defaults

## License

[MIT](license.md)
