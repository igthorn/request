
"use strict";

var Request      = require("./request");
var RequestError = require("./requestError");

module.exports.validate = function (req, res, next) {

  var request = new Request();
  request.reset();

  try {

    request.inflate(req.query);

  } catch (err) {

    return res.status(400).json("Bad request");

  }

  req.igthornParsedRequest = request;

  req.getParsedRequest = function() {
    return this.igthornParsedRequest;
  }

  next();

};
