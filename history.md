1.0.2 / 2016-08-26
==================
* Update to not overwrite request.query, update to readme

1.0.0 / 2016-08-24
==================
* Initial release
