
"use strict";

let Request           = require("./../../");
let RequestMiddleware = Request.RequestMiddleware;
let assert            = require("chai").assert;

describe("RequestMiddleware", function() {

  describe("passing all required params", function () {

    it("should cause to parse request and call callback function", function (done) {

      let query = {
        conditions: {
          price: {
            "gt" : 15,
            "lte": 70
          },
          "category": [21, 45]
        }
      };

      let request = {
        query: query
      };

      // adding defaults
      query.count = true;
      query.limit = 50;
      query.storage = "default";

      let response = {};

      let next = function() {
        let parsedRequest = request.getParsedRequest();
        assert.ok(parsedRequest instanceof Request);
        assert.deepEqual(parsedRequest.export(), query);
        done();
      }

      RequestMiddleware.validate(request, response, next);
    });
  });
});